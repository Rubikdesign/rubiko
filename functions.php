<?php
/**
 * Rubiko define theme variable functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Rubiko
 * @since 1.0
 */

if ( ! defined( 'THEME_URI' ) )
define( 'THEME_URI', get_stylesheet_directory_uri() );

if ( ! defined( 'RUBIKO_FRAMEWORK' ) ){

    define( 'RUBIKO_FRAMEWORK', THEME_URI . '/framework' );
}

if ( ! defined( 'RUBIKO_INCLUDES'  ) ){
    define( 'RUBIKO_INCLUDES', THEME_URI . '/include' );
}

if ( ! defined( 'RUBIKO_PLUGINS' ) ){
    define( 'RUBIKO_PLUGINS', RUBIKO_INCLUDES . '/plugins' );
}

if ( ! defined( 'RUBIKO_ASSETS' ) ){
    define( 'RUBIKO_ASSETS', THEME_URI . '/assets' );
}

if ( ! defined( 'RUBIKO_JS' ) ){
    define( 'RUBIKO_JS', RUBIKO_ASSETS . '/js' );
}

if ( ! defined( 'RUBIKO_CSS' ) ){
    define( 'RUBIKO_CSS', RUBIKO_ASSETS . '/css' );
}

if ( ! defined( 'RUBIKO_FUNCTIONS' ) ){
    define( 'RUBIKO_FUNCTIONS', RUBIKO_INCLUDES . '/functions' );
}
if ( ! defined( 'RUBIKO_METABOX' ) ){
    define( 'RUBIKO_METABOX', RUBIKO_INCLUDES . '/meta-box' );
}

/**
 * Rubiko only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}


if ( !class_exists( 'ReduxFramework' ) && file_exists( RUBIKO_FRAMEWORK . '/Redux/ReduxCore/framework.php' ) ) {
    require_once( RUBIKO_FRAMEWORK . '/Redux/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( get_template_directory() . '/include/functions/rubiko_theme_options/options.php' ) ) {
    require_once( get_template_directory() . '/include/functions/rubiko_theme_options/options.php' );
}
if (  file_exists( get_template_directory() . '/include/meta-box/meta-box.php' ) ) {
require_once( get_template_directory() . '/include/meta-box/meta-box.php'); // Path to the plugin's main file

}

if (  file_exists( get_template_directory() . '/include/functions/rubiko_scripts/rubiko-scripts.php' ) ) {
require_once( get_template_directory() . '/include/functions/rubiko_scripts/rubiko-scripts.php'); // Path to the plugin's main file

}

if (  file_exists( get_template_directory() . '/include/functions/rubiko_plugins/rubiko-plugins.php' ) ) {
require_once( get_template_directory() . '/include/functions/rubiko_plugins/rubiko-plugins.php'); // Path to the plugin's main file

}

if (  file_exists( get_template_directory() . '/include/functions/rubiko_metabox/rubiko-metabox.php' ) ) {
require_once( get_template_directory() . '/include/functions/rubiko_metabox/rubiko-metabox.php' ); // Path to the plugin's main file

}

if (  file_exists( get_template_directory() . '/include/functions/rubiko_sidebars/rubiko-sidebars.php' ) ) {
require_once( get_template_directory() . '/include/functions/rubiko_sidebars/rubiko-sidebars.php' ); // Path to the plugin's main file

}

if (  file_exists( get_template_directory() . '/include/functions/rubiko_widgets/rubiko-widgets.php' ) ) {
require_once( get_template_directory() . '/include/functions/rubiko_widgets/rubiko-widgets.php' ); // Path to the plugin's main file

}


if (  file_exists( get_template_directory() . '/include/functions/functions/rubiko-functions.php' ) ) {
require_once( get_template_directory()  . '/include/functions/functions/rubiko-functions.php'); // Path to the plugin's main file

}
// global $theretailer_theme_options;

// Load Rubiko Post types
if ( file_exists( get_template_directory() . '/include/functions/rubiko_custom_post_types/rubiko-custom-post-types.php' ) ) {
    require_once( get_template_directory() . '/include/functions/rubiko_custom_post_types/rubiko-custom-post-types.php' );
}









