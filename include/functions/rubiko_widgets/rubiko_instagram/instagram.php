<?php
/**
 * Instagram Slider Widget
 *
 * Learn more: http://codex.wordpress.org/Widgets_API
 *
 */

// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}


function instagramSliderScript(){
?>
    <script>


    jQuery(document).ready(function () {
        jQuery('.instagram-slider').slick({

            slidesToShow: 4,
            slidesToScroll: 4,
            prevArrow: '<span class="instagram-slick-prev"></span>',
            nextArrow: '<span class="instagram-slick-next"></span>'
        });

    });

    </script>
<?php
}


function getInstagramUserImages( $user = '', $slice = 4 ) {

    // Sanitize input and get transient
    $user           = strtolower( $user );
    $sanitized_user = sanitize_title_with_dashes( $user );
    $transient_name     = 'karibu-instagram-feed-'. $sanitized_user .'-'. $slice;
    $instagram          = get_transient( $transient_name );



    // Fetch instagram items
    if ( ! $instagram ) {

        $remote = wp_remote_get( 'http://instagram.com/'. trim( $user ) );

        if ( is_wp_error( $remote ) ) {
            return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'karibu' ) );
        }

        if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
            return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'karibu' ) );
        }

        $shards      = explode( 'window._sharedData = ', $remote['body'] );
        $insta_json  = explode( ';</script>', $shards[1] );
        $insta_array = json_decode( $insta_json[0], TRUE );

        if ( ! $insta_array ) {
            return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'karibu' ) );
        }

        // Old style
        if ( isset( $insta_array['entry_data']['UserProfile'][0]['userMedia'] ) ) {
            $images = $insta_array['entry_data']['UserProfile'][0]['userMedia'];
            $type = 'old';

        }

        // New style
        elseif ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
            $images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
            $type = 'new';
        }

        // Invalid json data
        else {
            return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'karibu' ) );
        }

        // Invalid data
        if ( ! is_array( $images ) ) {
            return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'karibu' ) );
        }

        $instagram = array();

        switch ( $type ) {

            case 'old':

                foreach ( $images as $image ) {

                    if ( $image['user']['user'] == $user ) {
                        $image['link'] = preg_replace( "/^http:/i", "", $image['link'] );
                        $image['images']['thumbnail'] = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
                        $image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );
                        $image['images']['low_resolution'] = preg_replace( "/^http:/i", "", $image['images']['low_resolution'] );
                        $instagram[] = array(
                            'description' => $image['caption']['text'],
                            'link'        => $image['link'],
                            'time'        => $image['created_time'],
                            'comments'    => $image['comments']['count'],
                            'likes'       => $image['likes']['count'],
                            'thumbnail'   => $image['images']['thumbnail'],
                            'large'       => $image['images']['standard_resolution'],
                            'small'       => $image['images']['low_resolution'],
                            'type'        => $image['type'],
                        );
                    }
                }

            break;

            default:

                foreach ( $images as $image ) {

                    $image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
                    $image['display_src'] = preg_replace( '/^https?\:/i', '', $image['display_src'] );

                    $image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
                    $image['display_src'] = preg_replace( '/^https?\:/i', '', $image['display_src'] );

                    // handle both types of CDN url
                    if ( (strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
                        $image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
                        $image['small'] = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
                    } else {
                        $urlparts = wp_parse_url( $image['thumbnail_src'] );
                        $pathparts = explode( '/', $urlparts['path'] );
                        array_splice( $pathparts, 3, 0, array( 's160x160' ) );
                        $image['thumbnail'] = '//' . $urlparts['host'] . implode('/', $pathparts);
                        $pathparts[3] = 's320x320';
                        $image['small'] = '//' . $urlparts['host'] . implode('/', $pathparts);
                    }

                    $image['large'] = $image['thumbnail_src'];

                    if ( $image['is_video'] == true ) {
                        $type = 'video';
                    } else {
                        $type = 'image';
                    }

                    $instagram[] = array(
                        'description'   => esc_html__( 'Instagram Image', 'karibu' ),
                        'link'          => '//instagram.com/p/' . $image['code'],
                        'time'          => $image['date'],
                        'comments'      => $image['comments']['count'],
                        'likes'         => $image['likes']['count'],
                        'thumbnail_src' => isset( $image['thumbnail_src'] ) ? $image['thumbnail_src'] : '',
                        'display_src'   => $image['display_src'],
                        'thumbnail'     => $image['thumbnail'],
                        'small'         => $image['small'],
                        'large'         => $image['large'],
                        'original'      => $image['display_src'],
                        'type'          => $type,
                    );

                }

            break;

        }

    }

    // Return array
    if ( ! empty( $instagram )  ) {
        if ( ! is_array( $instagram ) && 1 != $instagram ) {
            $instagram = unserialize( $instagram );
        }
        if ( is_array( $instagram ) ) {
            return array_slice( $instagram, 0, $slice );
        }
    }

    // No images returned
    else {

        return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'karibu' ) );

    }

}



// Start widget class
if ( ! class_exists( 'Kribu_Instagram_Grid_Widget' ) ) {

    class Kribu_Instagram_Grid_Widget extends WP_Widget {

        /**
         * Register widget with WordPress.
         *
         * @since 1.0.0
         */
        function __construct() {


            $widget_ops = array(

                'classname' => 'instagram_widget',
                'customize_selective_refresh' => true,
                'description' => 'Karibu Instagram Widget',
            );


            parent::__construct( 'instagram_image_slider', __( 'Instagram Grid', 'karibu' ) , $widget_ops );

        }

        /**
         * Front-end display of widget.
         *
         * @since 1.0.0
         */
        public function widget( $args, $instance ) {

            // Define vars
            $output     = '';
            $ulclass='';
            $title      = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
            $user   = empty( $instance['user'] ) ? '' : $instance['user'];
            $number     = empty( $instance['number'] ) ? 9 : $instance['number'];
            $columns    = empty( $instance['columns'] ) ? '3' : $instance['columns'];
            $size       = empty( $instance['size'] ) ? 'thumbnail' : $instance['size'];
            $use_slider       = empty( $instance['use_slider'] ) ? 0 : $instance['use_slider'];

            // Prevent size issues
            if ( ! in_array( $size, array( 'thumbnail', 'small', 'large', 'original' ) ) ) {
                $size = 'thumbnail';
            }

            if ($use_slider==1) {

                $ulclass ="instagram-slider";
                add_action("wp_footer", 'instagramSliderScript');


                # code...
            }

            // Before widget hook
            $output .= $args['before_widget'];

            // Display widget title
            if ( $title ) {
                $output .= $args['before_title'] . $title . $args['after_title'];
            }



            // Display notice for user not added
            if ( ! $user ) {

                $output .= '<p>'. esc_html__( 'Please enter an instagram user for your widget.', 'karibu' ) .'</p>';

            } else {

                // Get instagram images
                $media_array = getInstagramUserImages( $user, $number );

                // Display error message
                if ( is_wp_error( $media_array ) ) {

                    $output .= strip_tags( $media_array->get_error_message() );

                }

                // Display instagram feed
                elseif ( is_array( $media_array ) ) {

                    // Set correct gap class


                    $output .= '<div class="instagram-karibu-widget">';

                        $output .= '<ul class="row instagram-gallery '.$ulclass.'">';

                        $count = 0;

                        foreach ( $media_array as $item ) {

                            $image = isset( $item['display_src'] ) ? $item['display_src'] : '';

                            if ( 'thumbnail' == $size ) {
                                $image = ! empty( $item['thumbnail_src'] ) ? $item['thumbnail_src'] : $image;
                                $image = ! empty( $item['thumbnail'] ) ? $item['thumbnail'] : $image;
                            } elseif ( 'small' == $size ) {
                                $image = ! empty( $item['small'] ) ? $item['small'] : $image;
                            } elseif ( 'large' == $size ) {
                                $image = ! empty( $item['large'] ) ? $item['large'] : $image;
                            } elseif ( 'original' == $size ) {
                                $image = ! empty( $item['original'] ) ? $item['original'] : $image;
                            }

                            if ( $image ) {

                                $count++;

                                if ( strpos( $item['link'], 'http' ) === false ) {
                                    $item['link'] = str_replace( '//', 'https://', $item['link'] );
                                }

                                $classes = 'col-xs-'. esc_attr( $columns );

                                $output .= '<li class="'. $classes .'" style="padding:0px;">';

                                    $output .= '<a href="'. esc_url( $item['link'] ) .'" title="'. esc_attr( $item['description'] ) .'" target="_BLAK">';

                                            $output .= '<img class="img-responsive" src="'. esc_url( $image ) .'"  alt="'. esc_attr( $item['description'] ) .'" />';

                                        $output .= '</a>';

                                    $output .= '</li>';

                                if ( $columns == $count ) {
                                    $count = 0;
                                }

                            }
                        }

                        $output .= '</ul>';

                    $output .= '</div>';

                }

            }

            $output .= $args['after_widget'];

            echo $output;
        }

        /**
         * Sanitize widget form values as they are saved.
         *
         * @since 1.0.0
         */
        public function update( $new_instance, $old_instance ) {
            $instance               = $old_instance;
            $instance['title']      = strip_tags( $new_instance['title'] );
            $instance['size']       = isset( $new_instance['size'] ) ? strip_tags( $new_instance['size'] ) : 'thumbnail';
            $instance['user']   = isset( $new_instance['user'] ) ? trim( strip_tags( $new_instance['user'] ) ) : '';
            $instance['number']     = ! empty( $new_instance['number'] ) ? intval( $new_instance['number'] ) : 9;
            $instance['columns']    = isset( $new_instance['columns'] ) ? intval( $new_instance['columns'] ) : '';
            $instance['use_slider']    = isset( $new_instance['use_slider'] ) ? intval( $new_instance['use_slider'] ) : 0;
            return $instance;
        }

        /**
         * Back-end widget form.
         *
         * @since 1.0.0
         */
        public function form( $instance ) {

            extract( wp_parse_args( ( array ) $instance, array(
                'title'      => '',
                'user'   => '',
                'number'     => '9',
                'columns'    => '3',
                'target'     => '_self',
                'size'       => 'thumbnail',
                'use_slider'       => 1,

            ) ) ); ?>

            <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'karibu' ); ?></label>
            <input class="karibu-instagram-field" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
            </p>
            <!-- User -->
            <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'user' ) ); ?>"><?php esc_html_e( 'user', 'karibu' ); ?>:
            <input class="karibu-instagram-field" id="<?php echo esc_attr( $this->get_field_id( 'user' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'user' ) ); ?>" type="text" value="<?php echo esc_attr( $user ); ?>" />
            </label>
            </p>

            <p><label for="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>"><?php esc_html_e( 'Size', 'karibu' ); ?>:</label>
                <select id="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'size' ) ); ?>" class="karibu-instagram-field">
                    <option value="thumbnail" <?php selected( 'thumbnail', $size ) ?>><?php esc_html_e( 'Thumbnail', 'karibu' ); ?></option>
                    <option value="small" <?php selected( 'small', $size ) ?>><?php esc_html_e( 'Small', 'karibu' ); ?></option>
                    <option value="large" <?php selected( 'large', $size ) ?>><?php esc_html_e( 'Large', 'karibu' ); ?></option>
                    <option value="original" <?php selected( 'original', $size ) ?>><?php esc_html_e( 'Original', 'karibu' ); ?></option>
                </select>
            </p>


            <p><label for="<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>"><?php esc_html_e( 'Columns', 'karibu' ); ?>:</label>
                <select id="<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'columns' ) ); ?>" class="karibu-instagram-field">
                        <option value="1" <?php selected( '1', $columns ) ?>><?php esc_html_e( '1', 'karibu' ); ?></option>
                        <option value="2" <?php selected( '2', $columns ) ?>><?php esc_html_e( '2', 'karibu' ); ?></option>
                        <option value="3" <?php selected( '3', $columns ) ?>><?php esc_html_e( '3', 'karibu' ); ?></option>
                        <option value="4" <?php selected( '4', $columns ) ?>><?php esc_html_e( '4', 'karibu' ); ?></option>
                </select>
            </p>


           <p>
               <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of photos', 'karibu' ); ?>:
               <input class="karibu-instagram-field" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
               </label>
               <small><?php esc_html_e( 'Max 12 items.', 'karibu' ); ?></small>
           </p>

           <p>
               <label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Use slider', 'karibu' ); ?>:
               <input class="karibu-instagram-field" id="<?php echo esc_attr( $this->get_field_id( 'use_slider' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'use_slider' ) ); ?>" type="checkbox" <?php if($use_slider==1){ echo"checked";} ?> value="1" />
               </label>

           </p>



            <p style="background:#f9f9f9;padding:10px;border:1px solid #ededed;"><strong><?php esc_html_e( 'Cache Notice', 'karibu' ); ?></strong>: <?php esc_html_e( 'The Instagram feed is refreshed every 2 hours.', 'karibu' ); ?></p>

            <?php
        }

    }

}
register_widget( 'Kribu_Instagram_Grid_Widget' );