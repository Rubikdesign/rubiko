<?php
// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}

/**
 * Rubiko define theme variable functions and definitions
  *
 * @package WordPress
 * @subpackage Rubiko
 * @since 1.0
 */


if ( ! class_exists('Rubiko_Portofolio_post_type') ) {

    /**
    * The Class
    */
    class Rubiko_Portofolio_post_type
    {
    /**
     * Get things started.

     */

        function __construct()
        {

            add_action( 'init', array( $this, 'rubiko_post_portofolio'), 0 );
            add_action( 'init', array( $this,'rubiko_portofolio_taxonomy'), 0 );
            add_action( 'init', array( $this,'rubiko_portofolio_categories'), 0 );

        }

        // Register Custom Post Type
        public static function   rubiko_post_portofolio() {

            $labels = array(
                'name'                  => _x( 'Projects', 'Post Type General Name', 'rubiko' ),
                'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'rubiko' ),
                'menu_name'             => __( 'Protofolio', 'rubiko' ),
                'name_admin_bar'        => __( 'Post Type', 'rubiko' ),
                'archives'              => __( 'Project Archives', 'rubiko' ),
                'attributes'            => __( 'Project Attributes', 'rubiko' ),
                'parent_item_colon'     => __( 'Project:', 'rubiko' ),
                'all_items'             => __( 'All Projects', 'rubiko' ),
                'add_new_item'          => __( 'Add New Project', 'rubiko' ),
                'add_new'               => __( 'Add New Project', 'rubiko' ),
                'new_item'              => __( 'New Project', 'rubiko' ),
                'edit_item'             => __( 'Edit Project', 'rubiko' ),
                'update_item'           => __( 'Update Project', 'rubiko' ),
                'view_item'             => __( 'View Project', 'rubiko' ),
                'view_items'            => __( 'View Projects', 'rubiko' ),
                'search_items'          => __( 'Search Project', 'rubiko' ),
                'not_found'             => __( 'Project Not found', 'rubiko' ),
                'not_found_in_trash'    => __( 'Project Not found in Trash', 'rubiko' ),
                'featured_image'        => __( 'Project Featured Image', 'rubiko' ),
                'set_featured_image'    => __( 'Set Project featured image', 'rubiko' ),
                'remove_featured_image' => __( 'Remove Project featured image', 'rubiko' ),
                'use_featured_image'    => __( 'Use as Project featured image', 'rubiko' ),
                'insert_into_item'      => __( 'Insert into portofolio', 'rubiko' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Project', 'rubiko' ),
                'items_list'            => __( 'Projects list', 'rubiko' ),
                'items_list_navigation' => __( 'Projects list navigation', 'rubiko' ),
                'filter_items_list'     => __( 'Filter Projects list', 'rubiko' ),
            );
            $rewrite = array(
                'slug'                  => 'portofolio',
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
            );
            $args = array(
                'label'                 => __( 'Project', 'rubiko' ),
                'description'           => __( 'Post Type Portofolio', 'rubiko' ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
                'taxonomies'            => array(  ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'dashicons-format-aside',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => 'portofolio',
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'post',
                'show_in_rest'          => true,
            );
            register_post_type( 'portofolio', $args );

        }

        // Register Tag Taxonomy
        public static function rubiko_portofolio_taxonomy() {

            $labels = array(
                'name'                       => _x( 'Project Tags', 'Tags', 'rubiko' ),
                'singular_name'              => _x( 'Tags', 'Tags Singular Name', 'rubiko' ),
                'menu_name'                  => __( 'Project Tags', 'rubiko' ),
                'all_items'                  => __( 'All Project Tags', 'rubiko' ),
                'parent_item'                => __( 'Project', 'rubiko' ),
                'parent_item_colon'          => __( 'Project :', 'rubiko' ),
                'new_item_name'              => __( 'New Project Tag', 'rubiko' ),
                'add_new_item'               => __( 'Add New Project Tag', 'rubiko' ),
                'edit_item'                  => __( 'Edit Project Tag', 'rubiko' ),
                'update_item'                => __( 'Update Project Tag', 'rubiko' ),
                'view_item'                  => __( 'View Project Tag', 'rubiko' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'rubiko' ),
                'add_or_remove_items'        => __( 'Add or remove Project Tags', 'rubiko' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'rubiko' ),
                'popular_items'              => __( 'Popular Project Tags', 'rubiko' ),
                'search_items'               => __( 'Search Project Tags', 'rubiko' ),
                'not_found'                  => __( 'Project Tags Not Found', 'rubiko' ),
                'no_terms'                   => __( 'No Project Tags', 'rubiko' ),
                'items_list'                 => __( 'Project Tags list', 'rubiko' ),
                'items_list_navigation'      => __( 'Project Tagst navigation', 'rubiko' ),
            );
            $args = array(
                'labels'                     => $labels,
                'hierarchical'               => false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'update_count_callback'      => array($this,'potofolio_tag_updated'),
                'show_in_rest'               => true,
            );
            register_taxonomy( 'portofolio_tags', array( 'portofolio' ), $args );

        }

        public static function potofolio_tag_updated(){


        }

        // Register Categories Taxonomy

        public static function rubiko_portofolio_categories() {

            $labels = array(
                'name'                       => _x( 'Project Categories', 'Taxonomy General Name', 'rubiko' ),
                'singular_name'              => _x( 'Project Category', 'Taxonomy Singular Name', 'rubiko' ),
                'menu_name'                  => __( 'Project Categories', 'rubiko' ),
                'all_items'                  => __( 'All Project Categories', 'rubiko' ),
                'parent_item'                => __( 'Project', 'rubiko' ),
                'parent_item_colon'          => __( 'Project :', 'rubiko' ),
                'new_item_name'              => __( 'New Project Category', 'rubiko' ),
                'add_new_item'               => __( 'Add New Project Category', 'rubiko' ),
                'edit_item'                  => __( 'Edit Project Category', 'rubiko' ),
                'update_item'                => __( 'Update Project Category', 'rubiko' ),
                'view_item'                  => __( 'View Project Category', 'rubiko' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'rubiko' ),
                'add_or_remove_items'        => __( 'Add or remove Project Categories', 'rubiko' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'rubiko' ),
                'popular_items'              => __( 'Popular Project Categories', 'rubiko' ),
                'search_items'               => __( 'Search Project Categories', 'rubiko' ),
                'not_found'                  => __( 'Project Tags Not Found', 'rubiko' ),
                'no_terms'                   => __( 'No Project Categories', 'rubiko' ),
                'items_list'                 => __( 'Project Categories list', 'rubiko' ),
                'items_list_navigation'      => __( 'Project Category navigation', 'rubiko' ),
            );
            $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'update_count_callback'      => array($this, 'potofolio_categories_updated'),
                'show_in_rest'               => true,
            );
            register_taxonomy( 'portofolio_categories', array( 'portofolio' ), $args );

        }
        public static function potofolio_categories_updated(){


        }

    }

    $portofolio = new Rubiko_Portofolio_post_type();
    # code...
}




