<?php
// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}

/**
 * Rubiko define theme variable functions and definitions
  *
 * @package WordPress
 * @subpackage Rubiko
 * @since 1.0
 */


if ( ! class_exists('Rubiko_Team_post_type') ) {

    /**
    * The Class
    */
    class Rubiko_Team_post_type
    {
    /**
     * Get things started.

     */

        function __construct()
        {

            add_action( 'init', array( $this, 'rubiko_post_member'), 0 );
            add_action( 'init', array( $this,'rubiko_members_categories'), 0 );

        }

        // Register Custom Post Type
        public static function   rubiko_post_member() {

            $labels = array(
                'name'                  => _x( 'Team', 'Post Type General Name', 'rubiko' ),
                'singular_name'         => _x( 'Member', 'Post Type Singular Name', 'rubiko' ),
                'menu_name'             => __( 'Team', 'rubiko' ),
                'name_admin_bar'        => __( 'Member', 'rubiko' ),
                'archives'              => __( 'Member Archives', 'rubiko' ),
                'attributes'            => __( 'Member Attributes', 'rubiko' ),
                'parent_item_colon'     => __( 'Member:', 'rubiko' ),
                'all_items'             => __( 'All Members', 'rubiko' ),
                'add_new_item'          => __( 'Add New Member', 'rubiko' ),
                'add_new'               => __( 'Add New Member', 'rubiko' ),
                'new_item'              => __( 'New Member', 'rubiko' ),
                'edit_item'             => __( 'Edit Member', 'rubiko' ),
                'update_item'           => __( 'Update Member', 'rubiko' ),
                'view_item'             => __( 'View Member', 'rubiko' ),
                'view_items'            => __( 'View Members', 'rubiko' ),
                'search_items'          => __( 'Search Member', 'rubiko' ),
                'not_found'             => __( 'Member Not found', 'rubiko' ),
                'not_found_in_trash'    => __( 'Member Not found in Trash', 'rubiko' ),
                'featured_image'        => __( 'Member Featured Image', 'rubiko' ),
                'set_featured_image'    => __( 'Set Member featured image', 'rubiko' ),
                'remove_featured_image' => __( 'Remove Member featured image', 'rubiko' ),
                'use_featured_image'    => __( 'Use as Member featured image', 'rubiko' ),
                'insert_into_item'      => __( 'Insert into portofolio', 'rubiko' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Member', 'rubiko' ),
                'items_list'            => __( 'Members list', 'rubiko' ),
                'items_list_navigation' => __( 'Members list navigation', 'rubiko' ),
                'filter_items_list'     => __( 'Filter Members list', 'rubiko' ),
            );
            $rewrite = array(
                'slug'                  => 'team',
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
            );
            $args = array(
                'label'                 => __( 'Member', 'rubiko' ),
                'description'           => __( 'Post Type Team', 'rubiko' ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
                'taxonomies'            => array(  ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'dashicons-universal-access',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => 'members',
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'post',
                'show_in_rest'          => true,
            );
            register_post_type( 'members', $args );

        }


        // Register Categories Taxonomy

        public static function rubiko_members_categories() {

            $labels = array(
                'name'                       => _x( 'Member Categories', 'Taxonomy General Name', 'rubiko' ),
                'singular_name'              => _x( 'Member Category', 'Taxonomy Singular Name', 'rubiko' ),
                'menu_name'                  => __( 'Member Categories', 'rubiko' ),
                'all_items'                  => __( 'All Member Categories', 'rubiko' ),
                'parent_item'                => __( 'Member', 'rubiko' ),
                'parent_item_colon'          => __( 'Member :', 'rubiko' ),
                'new_item_name'              => __( 'New Member Category', 'rubiko' ),
                'add_new_item'               => __( 'Add New Member Category', 'rubiko' ),
                'edit_item'                  => __( 'Edit Member Category', 'rubiko' ),
                'update_item'                => __( 'Update Member Category', 'rubiko' ),
                'view_item'                  => __( 'View Member Category', 'rubiko' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'rubiko' ),
                'add_or_remove_items'        => __( 'Add or remove Member Categories', 'rubiko' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'rubiko' ),
                'popular_items'              => __( 'Popular Member Categories', 'rubiko' ),
                'search_items'               => __( 'Search Member Categories', 'rubiko' ),
                'not_found'                  => __( 'Member Tags Not Found', 'rubiko' ),
                'no_terms'                   => __( 'No Member Categories', 'rubiko' ),
                'items_list'                 => __( 'Member Categories list', 'rubiko' ),
                'items_list_navigation'      => __( 'Member Category navigation', 'rubiko' ),
            );
            $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'update_count_callback'      =>'',
                'show_in_rest'               => true,
            );
            register_taxonomy( 'members_categories', array( 'members' ), $args );

        }

    }

    $portofolio = new Rubiko_Team_post_type();
    # code...
}




