<?php
// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}

if ( file_exists( get_template_directory() . '/include/functions/rubiko_custom_post_types/portofolio/rubiko-custom-post-portofolio.php' ) ) {
    require_once( get_template_directory() . '/include/functions/rubiko_custom_post_types/portofolio/rubiko-custom-post-portofolio.php' );
}

if ( file_exists( get_template_directory() . '/include/functions/rubiko_custom_post_types/testimonials/rubiko-custom-post-testimonials.php' ) ) {
    require_once( get_template_directory() . '/include/functions/rubiko_custom_post_types/testimonials/rubiko-custom-post-testimonials.php' );
}

if ( file_exists( get_template_directory() . '/include/functions/rubiko_custom_post_types/team/rubiko-custom-post-team.php' ) ) {
    require_once( get_template_directory() . '/include/functions/rubiko_custom_post_types/team/rubiko-custom-post-team.php' );
}
