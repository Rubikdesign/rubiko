<?php
// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}

/**
 * Rubiko define theme variable functions and definitions
  *
 * @package WordPress
 * @subpackage Rubiko
 * @since 1.0
 */


if ( ! class_exists('Rubiko_Testimonial_post_type') ) {

    /**
    * The Class
    */
    class Rubiko_Testimonial_post_type
    {
    /**
     * Get things started.

     */

        function __construct()
        {

            add_action( 'init', array( $this, 'rubiko_post_testimonial'), 0 );
            add_action( 'init', array( $this,'rubiko_testimonial_categories'), 0 );

        }

        // Register Custom Post Type
        public static function   rubiko_post_testimonial() {

            $labels = array(
                'name'                  => _x( 'Testimonials', 'Post Type General Name', 'rubiko' ),
                'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'rubiko' ),
                'menu_name'             => __( 'Testimonials', 'rubiko' ),
                'name_admin_bar'        => __( 'Testimonial', 'rubiko' ),
                'archives'              => __( 'Testimonial Archives', 'rubiko' ),
                'attributes'            => __( 'Testimonial Attributes', 'rubiko' ),
                'parent_item_colon'     => __( 'Testimonial:', 'rubiko' ),
                'all_items'             => __( 'All Testimonials', 'rubiko' ),
                'add_new_item'          => __( 'Add New Testimonial', 'rubiko' ),
                'add_new'               => __( 'Add New Testimonial', 'rubiko' ),
                'new_item'              => __( 'New Testimonial', 'rubiko' ),
                'edit_item'             => __( 'Edit Testimonial', 'rubiko' ),
                'update_item'           => __( 'Update Testimonial', 'rubiko' ),
                'view_item'             => __( 'View Testimonial', 'rubiko' ),
                'view_items'            => __( 'View Testimonials', 'rubiko' ),
                'search_items'          => __( 'Search Testimonial', 'rubiko' ),
                'not_found'             => __( 'Testimonial Not found', 'rubiko' ),
                'not_found_in_trash'    => __( 'Testimonial Not found in Trash', 'rubiko' ),
                'featured_image'        => __( 'Testimonial Featured Image', 'rubiko' ),
                'set_featured_image'    => __( 'Set Testimonial featured image', 'rubiko' ),
                'remove_featured_image' => __( 'Remove Testimonial featured image', 'rubiko' ),
                'use_featured_image'    => __( 'Use as Testimonial featured image', 'rubiko' ),
                'insert_into_item'      => __( 'Insert into portofolio', 'rubiko' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'rubiko' ),
                'items_list'            => __( 'Testimonials list', 'rubiko' ),
                'items_list_navigation' => __( 'Testimonials list navigation', 'rubiko' ),
                'filter_items_list'     => __( 'Filter Testimonials list', 'rubiko' ),
            );
            $rewrite = array(
                'slug'                  => 'testimonials',
                'with_front'            => true,
                'pages'                 => true,
                'feeds'                 => true,
            );
            $args = array(
                'label'                 => __( 'Testimonial', 'rubiko' ),
                'description'           => __( 'Post Type Testimonials', 'rubiko' ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
                'taxonomies'            => array(  ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'menu_icon'             => 'dashicons-testimonial',
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => 'testimonials',
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'rewrite'               => $rewrite,
                'capability_type'       => 'post',
                'show_in_rest'          => true,
            );
            register_post_type( 'testimonials', $args );

        }


        // Register Categories Taxonomy

        public static function rubiko_testimonial_categories() {

            $labels = array(
                'name'                       => _x( 'Testimonial Categories', 'Taxonomy General Name', 'rubiko' ),
                'singular_name'              => _x( 'Testimonial Category', 'Taxonomy Singular Name', 'rubiko' ),
                'menu_name'                  => __( 'Testimonial Categories', 'rubiko' ),
                'all_items'                  => __( 'All Testimonial Categories', 'rubiko' ),
                'parent_item'                => __( 'Testimonial', 'rubiko' ),
                'parent_item_colon'          => __( 'Testimonial :', 'rubiko' ),
                'new_item_name'              => __( 'New Testimonial Category', 'rubiko' ),
                'add_new_item'               => __( 'Add New Testimonial Category', 'rubiko' ),
                'edit_item'                  => __( 'Edit Testimonial Category', 'rubiko' ),
                'update_item'                => __( 'Update Testimonial Category', 'rubiko' ),
                'view_item'                  => __( 'View Testimonial Category', 'rubiko' ),
                'separate_items_with_commas' => __( 'Separate items with commas', 'rubiko' ),
                'add_or_remove_items'        => __( 'Add or remove Testimonial Categories', 'rubiko' ),
                'choose_from_most_used'      => __( 'Choose from the most used', 'rubiko' ),
                'popular_items'              => __( 'Popular Testimonial Categories', 'rubiko' ),
                'search_items'               => __( 'Search Testimonial Categories', 'rubiko' ),
                'not_found'                  => __( 'Testimonial Tags Not Found', 'rubiko' ),
                'no_terms'                   => __( 'No Testimonial Categories', 'rubiko' ),
                'items_list'                 => __( 'Testimonial Categories list', 'rubiko' ),
                'items_list_navigation'      => __( 'Testimonial Category navigation', 'rubiko' ),
            );
            $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
                'update_count_callback'      =>'',
                'show_in_rest'               => true,
            );
            register_taxonomy( 'testimonials_categories', array( 'testimonials' ), $args );

        }


    }

    $portofolio = new Rubiko_Testimonial_post_type();
    # code...
}




