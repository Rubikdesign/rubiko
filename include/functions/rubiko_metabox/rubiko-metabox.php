<?php
add_filter( 'rwmb_meta_boxes', 'your_prefix_meta_boxes' );

function your_prefix_meta_boxes( $meta_boxes ) {


    $meta_boxes[] = array(
        'title'      => __( 'Test Meta Box', 'rubiko' ),
        'post_types' => 'post',
        'fields'     => array(
            array(
                'id'   => 'name',
                'name' => __( 'Name', 'rubiko' ),
                'type' => 'text',
            ),
            array(
                'id'      => 'gender',
                'name'    => __( 'Gender', 'rubiko' ),
                'type'    => 'radio',
                'options' => array(
                    'm' => __( 'Male', 'rubiko' ),
                    'f' => __( 'Female', 'rubiko' ),
                ),
            ),
            array(
                'id'   => 'email',
                'name' => __( 'Email', 'rubiko' ),
                'type' => 'email',
            ),
            array(
                'id'   => 'bio',
                'name' => __( 'Biography', 'rubiko' ),
                'type' => 'textarea',
            ),
        ),
    );
    return $meta_boxes;
}