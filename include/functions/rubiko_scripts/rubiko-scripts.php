<?php

/**
 * Enqueue scripts and styles.
 */
function rubiko_scripts() {

    // // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style( 'rubiko-fonts', rubiko_fonts_url(), array(), null );

    // // Theme stylesheet.
    wp_enqueue_style( 'rubiko-style', get_stylesheet_uri() );


    // Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
    if ( is_customize_preview() ) {
        wp_enqueue_style( 'rubiko-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'rubiko-style' ), '1.0' );
        wp_style_add_data( 'rubiko-ie9', 'conditional', 'IE 9' );
    }

    // Load the Internet Explorer 8 specific stylesheet.
    wp_enqueue_style( 'rubiko-ie8', RUBIKO_CSS. 'ie8.css' , array( 'rubiko-style' ), '1.0' );
    wp_style_add_data( 'rubiko-ie8', 'conditional', 'lt IE 9' );

    // // Load the html5 shiv.
    wp_enqueue_script( 'html5', RUBIKO_JS.  'html5.js' , array(), '3.7.3' );
    wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );


    // if ( has_nav_menu( 'top' ) ) {
    //  wp_enqueue_script( 'rubiko-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );

    // }


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'rubiko_scripts' );
