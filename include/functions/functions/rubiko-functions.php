<?php
// Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rubiko_setup() {
  /*
   * Make theme available for translation.
   * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/rubiko
   * If you're building a theme based on Rubiko, use a find and replace
   * to change 'rubiko' to the name of your theme in all the template files.
   */
  load_theme_textdomain( 'rubiko' );

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support( 'post-thumbnails' );

  add_image_size( 'rubiko-featured-image', 2000, 1200, true );

  add_image_size( 'rubiko-thumbnail-avatar', 100, 100, true );

  // Set the default content width.
  $GLOBALS['content_width'] = 525;

  // This theme uses wp_nav_menu() in two locations.
  register_nav_menus( array(
    'top'    => __( 'Top Menu', 'rubiko' ),
    'social' => __( 'Social Links Menu', 'rubiko' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

  /*
   * Enable support for Post Formats.
   *
   * See: https://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
    'gallery',
    'audio',
  ) );

  // Add theme support for Custom Logo.
  add_theme_support( 'custom-logo', array(
    'width'       => 250,
    'height'      => 250,
    'flex-width'  => true,
  ) );

}
add_action( 'after_setup_theme', 'rubiko_setup' );


/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Rubiko 1.0
 */
function rubiko_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'rubiko_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function rubiko_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
    }
}
add_action( 'wp_head', 'rubiko_pingback_header' );

#-----------------------------------------------------------------#
# Rubiko CustomPagination
#-----------------------------------------------------------------#

if ( !function_exists( 'rubiko_get_pagination' ) ) {

    function rubiko_get_pagination() {

        global $options;
        //extra pagination
        if( !empty($options['extra_pagination']) && $options['extra_pagination'] == '1' ){

                global $wp_query, $wp_rewrite;

                $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
                $total_pages = $wp_query->max_num_pages;

                if ($total_pages > 1){

                  $permalink_structure = get_option('permalink_structure');
                  $query_type = (count($_GET)) ? '&' : '?';
                  $format = empty( $permalink_structure ) ? $query_type.'paged=%#%' : 'page/%#%/';

                  echo '<div id="pagination" data-is-text="'.__("All items loaded", 'rubiko').'">';

                  echo paginate_links(array(
                      'base' => get_pagenum_link(1) . '%_%',
                      'format' => $format,
                      'current' => $current,
                      'total' => $total_pages,
                      'prev_text'    => __('Previous','rubiko'),
                      'next_text'    => __('Next','rubiko')
                    ));

                  echo  '</div>';

                }
        }
        //regular pagination
        else{

            if( get_next_posts_link() || get_previous_posts_link() ) {
                echo '<div id="pagination" data-is-text="'.__("All items loaded", 'rubiko').'">
                      <div class="prev">'.get_previous_posts_link('&laquo; Previous').'</div>
                      <div class="next">'.get_next_posts_link('NextPrevious &raquo;','').'</div>
                      </div>';

            }
        }


    }

}


/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Rubiko 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function rubiko_excerpt_more( $link ) {
    if ( is_admin() ) {
        return $link;
    }

    $link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
        esc_url( get_permalink( get_the_ID() ) ),
        /* translators: %s: Name of current post */
        sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'rubiko' ), get_the_title( get_the_ID() ) )
    );
    return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'rubiko_excerpt_more' );


#-----------------------------------------------------------------#
# Dynamic Styles
#-----------------------------------------------------------------#

if (!function_exists('rubiko_css_minify')) {

    function rubiko_css_minify( $minifycss ) {

        $minifycss = preg_replace( '/\s+/', ' ', $minifycss );
        $minifycss = preg_replace( '/\/\*[^\!](.*?)\*\//', '', $minifycss );
        $minifycss = preg_replace( '/(,|:|;|\{|}) /', '$1', $minifycss );
        $minifycss = preg_replace( '/ (,|;|\{|})/', '$1', $minifycss );
        $minifycss = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $minifycss );
        $minifycss = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $minifycss );
        return trim( $css );

    }

}

// *************************************************************************************
/* Human time */
// *************************************************************************************

if ( !function_exists( 'rubiko_get_human_time' ) ) {

    function rubiko_human_time( $duration = 60 ) {

        $the_post_time = get_the_time('U');
        $human_time = '';

        $time_now = date('U');

        // use human time if less that $duration days ago (60 days by default)
        // 60 seconds * 60 minutes * 24 hours * $duration days
        if ( $the_post_time > $time_now - ( 60 * 60 * 24 * $duration ) ) {
            $human_time = sprintf( __( '%s ago', 'rubiko'), human_time_diff( $the_post_time, current_time( 'timestamp' ) ) );
        } else {
            $human_time = get_the_date();
        }

        return $human_time;

    }
}