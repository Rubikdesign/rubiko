<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_demo";


    $rubiko_font_faces = array('arial'=>'Arial',
        'verdana'=>'Verdana, Geneva',
        'trebuchet'=>'Trebuchet MS',
        'georgia' =>'Georgia',
        'times'=>'Times New Roman',
        'tahoma'=>'Tahoma, Geneva',
        'helvetica'=>'Helvetica',
        'ABeeZee' => 'ABeeZee',
        'Abel' => 'Abel',
        'Abril Fatface' => 'Abril Fatface',
        'Aclonica' => 'Aclonica',
        'Acme' => 'Acme',
        'Actor' => 'Actor',
        'Adamina' => 'Adamina',
        'Advent Pro' => 'Advent Pro',
        'Aguafina Script' => 'Aguafina Script',
        'Akronim' => 'Akronim',
        'Aladin' => 'Aladin',
        'Aldrich' => 'Aldrich',
        'Alef' => 'Alef',
        'Alegreya' => 'Alegreya',
        'Alegreya SC' => 'Alegreya SC',
        'Alegreya Sans' => 'Alegreya Sans',
        'Alegreya Sans SC' => 'Alegreya Sans SC',
        'Alex Brush' => 'Alex Brush',
        'Alfa Slab One' => 'Alfa Slab One',
        'Alice' => 'Alice',
        'Alike' => 'Alike',
        'Alike Angular' => 'Alike Angular',
        'Allan' => 'Allan',
        'Allerta' => 'Allerta',
        'Allerta Stencil' => 'Allerta Stencil',
        'Allura' => 'Allura',
        'Almendra' => 'Almendra',
        'Almendra Display' => 'Almendra Display',
        'Almendra SC' => 'Almendra SC',
        'Amarante' => 'Amarante',
        'Amaranth' => 'Amaranth',
        'Amatic SC' => 'Amatic SC',
        'Amethysta' => 'Amethysta',
        'Anaheim' => 'Anaheim',
        'Andada' => 'Andada',
        'Andika' => 'Andika',
        'Angkor' => 'Angkor',
        'Annie Use Your Telescope' => 'Annie Use Your Telescope',
        'Anonymous Pro' => 'Anonymous Pro',
        'Antic' => 'Antic',
        'Antic Didone' => 'Antic Didone',
        'Antic Slab' => 'Antic Slab',
        'Anton' => 'Anton',
        'Arapey' => 'Arapey',
        'Arbutus' => 'Arbutus',
        'Arbutus Slab' => 'Arbutus Slab',
        'Architects Daughter' => 'Architects Daughter',
        'Archivo Black' => 'Archivo Black',
        'Archivo Narrow' => 'Archivo Narrow',
        'Arimo' => 'Arimo',
        'Arizonia' => 'Arizonia',
        'Armata' => 'Armata',
        'Artifika' => 'Artifika',
        'Arvo' => 'Arvo',
        'Asap' => 'Asap',
        'Asset' => 'Asset',
        'Astloch' => 'Astloch',
        'Asul' => 'Asul',
        'Atomic Age' => 'Atomic Age',
        'Aubrey' => 'Aubrey',
        'Audiowide' => 'Audiowide',
        'Autour One' => 'Autour One',
        'Average' => 'Average',
        'Average Sans' => 'Average Sans',
        'Averia Gruesa Libre' => 'Averia Gruesa Libre',
        'Averia Libre' => 'Averia Libre',
        'Averia Sans Libre' => 'Averia Sans Libre',
        'Averia Serif Libre' => 'Averia Serif Libre',
        'Bad Script' => 'Bad Script',
        'Balthazar' => 'Balthazar',
        'Bangers' => 'Bangers',
        'Basic' => 'Basic',
        'Battambang' => 'Battambang',
        'Baumans' => 'Baumans',
        'Bayon' => 'Bayon',
        'Belgrano' => 'Belgrano',
        'Belleza' => 'Belleza',
        'BenchNine' => 'BenchNine',
        'Bentham' => 'Bentham',
        'Berkshire Swash' => 'Berkshire Swash',
        'Bevan' => 'Bevan',
        'Bigelow Rules' => 'Bigelow Rules',
        'Bigshot One' => 'Bigshot One',
        'Bilbo' => 'Bilbo',
        'Bilbo Swash Caps' => 'Bilbo Swash Caps',
        'Bitter' => 'Bitter',
        'Black Ops One' => 'Black Ops One',
        'Bokor' => 'Bokor',
        'Bonbon' => 'Bonbon',
        'Boogaloo' => 'Boogaloo',
        'Bowlby One' => 'Bowlby One',
        'Bowlby One SC' => 'Bowlby One SC',
        'Brawler' => 'Brawler',
        'Bree Serif' => 'Bree Serif',
        'Bubblegum Sans' => 'Bubblegum Sans',
        'Bubbler One' => 'Bubbler One',
        'Buda' => 'Buda',
        'Buenard' => 'Buenard',
        'Butcherman' => 'Butcherman',
        'Butterfly Kids' => 'Butterfly Kids',
        'Cabin' => 'Cabin',
        'Cabin Condensed' => 'Cabin Condensed',
        'Cabin Sketch' => 'Cabin Sketch',
        'Caesar Dressing' => 'Caesar Dressing',
        'Cagliostro' => 'Cagliostro',
        'Calligraffitti' => 'Calligraffitti',
        'Cambo' => 'Cambo',
        'Candal' => 'Candal',
        'Cantarell' => 'Cantarell',
        'Cantata One' => 'Cantata One',
        'Cantora One' => 'Cantora One',
        'Capriola' => 'Capriola',
        'Cardo' => 'Cardo',
        'Carme' => 'Carme',
        'Carrois Gothic' => 'Carrois Gothic',
        'Carrois Gothic SC' => 'Carrois Gothic SC',
        'Carter One' => 'Carter One',
        'Caudex' => 'Caudex',
        'Cedarville Cursive' => 'Cedarville Cursive',
        'Ceviche One' => 'Ceviche One',
        'Changa One' => 'Changa One',
        'Chango' => 'Chango',
        'Chau Philomene One' => 'Chau Philomene One',
        'Chela One' => 'Chela One',
        'Chelsea Market' => 'Chelsea Market',
        'Chenla' => 'Chenla',
        'Cherry Cream Soda' => 'Cherry Cream Soda',
        'Cherry Swash' => 'Cherry Swash',
        'Chewy' => 'Chewy',
        'Chicle' => 'Chicle',
        'Chivo' => 'Chivo',
        'Cinzel' => 'Cinzel',
        'Cinzel Decorative' => 'Cinzel Decorative',
        'Clicker Script' => 'Clicker Script',
        'Coda' => 'Coda',
        'Coda Caption' => 'Coda Caption',
        'Codystar' => 'Codystar',
        'Combo' => 'Combo',
        'Comfortaa' => 'Comfortaa',
        'Coming Soon' => 'Coming Soon',
        'Concert One' => 'Concert One',
        'Condiment' => 'Condiment',
        'Content' => 'Content',
        'Contrail One' => 'Contrail One',
        'Convergence' => 'Convergence',
        'Cookie' => 'Cookie',
        'Copse' => 'Copse',
        'Corben' => 'Corben',
        'Courgette' => 'Courgette',
        'Cousine' => 'Cousine',
        'Coustard' => 'Coustard',
        'Covered By Your Grace' => 'Covered By Your Grace',
        'Crafty Girls' => 'Crafty Girls',
        'Creepster' => 'Creepster',
        'Crete Round' => 'Crete Round',
        'Crimson Text' => 'Crimson Text',
        'Croissant One' => 'Croissant One',
        'Crushed' => 'Crushed',
        'Cuprum' => 'Cuprum',
        'Cutive' => 'Cutive',
        'Cutive Mono' => 'Cutive Mono',
        'Damion' => 'Damion',
        'Dancing Script' => 'Dancing Script',
        'Dangrek' => 'Dangrek',
        'Dawning of a New Day' => 'Dawning of a New Day',
        'Days One' => 'Days One',
        'Delius' => 'Delius',
        'Delius Swash Caps' => 'Delius Swash Caps',
        'Delius Unicase' => 'Delius Unicase',
        'Della Respira' => 'Della Respira',
        'Denk One' => 'Denk One',
        'Devonshire' => 'Devonshire',
        'Didact Gothic' => 'Didact Gothic',
        'Diplomata' => 'Diplomata',
        'Diplomata SC' => 'Diplomata SC',
        'Domine' => 'Domine',
        'Donegal One' => 'Donegal One',
        'Doppio One' => 'Doppio One',
        'Dorsa' => 'Dorsa',
        'Dosis' => 'Dosis',
        'Dr Sugiyama' => 'Dr Sugiyama',
        'Droid Sans' => 'Droid Sans',
        'Droid Sans Mono' => 'Droid Sans Mono',
        'Droid Serif' => 'Droid Serif',
        'Duru Sans' => 'Duru Sans',
        'Dynalight' => 'Dynalight',
        'EB Garamond' => 'EB Garamond',
        'Eagle Lake' => 'Eagle Lake',
        'Eater' => 'Eater',
        'Economica' => 'Economica',
        'Ek Mukta' => 'Ek Mukta',
        'Electrolize' => 'Electrolize',
        'Elsie' => 'Elsie',
        'Elsie Swash Caps' => 'Elsie Swash Caps',
        'Emblema One' => 'Emblema One',
        'Emilys Candy' => 'Emilys Candy',
        'Engagement' => 'Engagement',
        'Englebert' => 'Englebert',
        'Enriqueta' => 'Enriqueta',
        'Erica One' => 'Erica One',
        'Esteban' => 'Esteban',
        'Euphoria Script' => 'Euphoria Script',
        'Ewert' => 'Ewert',
        'Exo' => 'Exo',
        'Exo 2' => 'Exo 2',
        'Expletus Sans' => 'Expletus Sans',
        'Fanwood Text' => 'Fanwood Text',
        'Fascinate' => 'Fascinate',
        'Fascinate Inline' => 'Fascinate Inline',
        'Faster One' => 'Faster One',
        'Fasthand' => 'Fasthand',
        'Fauna One' => 'Fauna One',
        'Federant' => 'Federant',
        'Federo' => 'Federo',
        'Felipa' => 'Felipa',
        'Fenix' => 'Fenix',
        'Finger Paint' => 'Finger Paint',
        'Fira Mono' => 'Fira Mono',
        'Fira Sans' => 'Fira Sans',
        'Fjalla One' => 'Fjalla One',
        'Fjord One' => 'Fjord One',
        'Flamenco' => 'Flamenco',
        'Flavors' => 'Flavors',
        'Fondamento' => 'Fondamento',
        'Fontdiner Swanky' => 'Fontdiner Swanky',
        'Forum' => 'Forum',
        'Francois One' => 'Francois One',
        'Freckle Face' => 'Freckle Face',
        'Fredericka the Great' => 'Fredericka the Great',
        'Fredoka One' => 'Fredoka One',
        'Freehand' => 'Freehand',
        'Fresca' => 'Fresca',
        'Frijole' => 'Frijole',
        'Fruktur' => 'Fruktur',
        'Fugaz One' => 'Fugaz One',
        'GFS Didot' => 'GFS Didot',
        'GFS Neohellenic' => 'GFS Neohellenic',
        'Gabriela' => 'Gabriela',
        'Gafata' => 'Gafata',
        'Galdeano' => 'Galdeano',
        'Galindo' => 'Galindo',
        'Gentium Basic' => 'Gentium Basic',
        'Gentium Book Basic' => 'Gentium Book Basic',
        'Geo' => 'Geo',
        'Geostar' => 'Geostar',
        'Geostar Fill' => 'Geostar Fill',
        'Germania One' => 'Germania One',
        'Gilda Display' => 'Gilda Display',
        'Give You Glory' => 'Give You Glory',
        'Glass Antiqua' => 'Glass Antiqua',
        'Glegoo' => 'Glegoo',
        'Gloria Hallelujah' => 'Gloria Hallelujah',
        'Goblin One' => 'Goblin One',
        'Gochi Hand' => 'Gochi Hand',
        'Gorditas' => 'Gorditas',
        'Goudy Bookletter 1911' => 'Goudy Bookletter 1911',
        'Graduate' => 'Graduate',
        'Grand Hotel' => 'Grand Hotel',
        'Gravitas One' => 'Gravitas One',
        'Great Vibes' => 'Great Vibes',
        'Griffy' => 'Griffy',
        'Gruppo' => 'Gruppo',
        'Gudea' => 'Gudea',
        'Habibi' => 'Habibi',
        'Hammersmith One' => 'Hammersmith One',
        'Hanalei' => 'Hanalei',
        'Hanalei Fill' => 'Hanalei Fill',
        'Handlee' => 'Handlee',
        'Hanuman' => 'Hanuman',
        'Happy Monkey' => 'Happy Monkey',
        'Headland One' => 'Headland One',
        'Henny Penny' => 'Henny Penny',
        'Herr Von Muellerhoff' => 'Herr Von Muellerhoff',
        'Holtwood One SC' => 'Holtwood One SC',
        'Homemade Apple' => 'Homemade Apple',
        'Homenaje' => 'Homenaje',
        'IM Fell DW Pica' => 'IM Fell DW Pica',
        'IM Fell DW Pica SC' => 'IM Fell DW Pica SC',
        'IM Fell Double Pica' => 'IM Fell Double Pica',
        'IM Fell Double Pica SC' => 'IM Fell Double Pica SC',
        'IM Fell English' => 'IM Fell English',
        'IM Fell English SC' => 'IM Fell English SC',
        'IM Fell French Canon' => 'IM Fell French Canon',
        'IM Fell French Canon SC' => 'IM Fell French Canon SC',
        'IM Fell Great Primer' => 'IM Fell Great Primer',
        'IM Fell Great Primer SC' => 'IM Fell Great Primer SC',
        'Iceberg' => 'Iceberg',
        'Iceland' => 'Iceland',
        'Imprima' => 'Imprima',
        'Inconsolata' => 'Inconsolata',
        'Inder' => 'Inder',
        'Indie Flower' => 'Indie Flower',
        'Inika' => 'Inika',
        'Irish Grover' => 'Irish Grover',
        'Istok Web' => 'Istok Web',
        'Italiana' => 'Italiana',
        'Italianno' => 'Italianno',
        'Jacques Francois' => 'Jacques Francois',
        'Jacques Francois Shadow' => 'Jacques Francois Shadow',
        'Jim Nightshade' => 'Jim Nightshade',
        'Jockey One' => 'Jockey One',
        'Jolly Lodger' => 'Jolly Lodger',
        'Josefin Sans' => 'Josefin Sans',
        'Josefin Slab' => 'Josefin Slab',
        'Joti One' => 'Joti One',
        'Judson' => 'Judson',
        'Julee' => 'Julee',
        'Julius Sans One' => 'Julius Sans One',
        'Junge' => 'Junge',
        'Jura' => 'Jura',
        'Just Another Hand' => 'Just Another Hand',
        'Just Me Again Down Here' => 'Just Me Again Down Here',
        'Kameron' => 'Kameron',
        'Kantumruy' => 'Kantumruy',
        'Karla' => 'Karla',
        'Kaushan Script' => 'Kaushan Script',
        'Kavoon' => 'Kavoon',
        'Kdam Thmor' => 'Kdam Thmor',
        'Keania One' => 'Keania One',
        'Kelly Slab' => 'Kelly Slab',
        'Kenia' => 'Kenia',
        'Khmer' => 'Khmer',
        'Kite One' => 'Kite One',
        'Knewave' => 'Knewave',
        'Kotta One' => 'Kotta One',
        'Koulen' => 'Koulen',
        'Kranky' => 'Kranky',
        'Kreon' => 'Kreon',
        'Kristi' => 'Kristi',
        'Krona One' => 'Krona One',
        'La Belle Aurore' => 'La Belle Aurore',
        'Lancelot' => 'Lancelot',
        'Lato' => 'Lato',
        'League Script' => 'League Script',
        'Leckerli One' => 'Leckerli One',
        'Ledger' => 'Ledger',
        'Lekton' => 'Lekton',
        'Lemon' => 'Lemon',
        'Libre Baskerville' => 'Libre Baskerville',
        'Life Savers' => 'Life Savers',
        'Lilita One' => 'Lilita One',
        'Lily Script One' => 'Lily Script One',
        'Limelight' => 'Limelight',
        'Linden Hill' => 'Linden Hill',
        'Lobster' => 'Lobster',
        'Lobster Two' => 'Lobster Two',
        'Londrina Outline' => 'Londrina Outline',
        'Londrina Shadow' => 'Londrina Shadow',
        'Londrina Sketch' => 'Londrina Sketch',
        'Londrina Solid' => 'Londrina Solid',
        'Lora' => 'Lora',
        'Love Ya Like A Sister' => 'Love Ya Like A Sister',
        'Loved by the King' => 'Loved by the King',
        'Lovers Quarrel' => 'Lovers Quarrel',
        'Luckiest Guy' => 'Luckiest Guy',
        'Lusitana' => 'Lusitana',
        'Lustria' => 'Lustria',
        'Macondo' => 'Macondo',
        'Macondo Swash Caps' => 'Macondo Swash Caps',
        'Magra' => 'Magra',
        'Maiden Orange' => 'Maiden Orange',
        'Mako' => 'Mako',
        'Marcellus' => 'Marcellus',
        'Marcellus SC' => 'Marcellus SC',
        'Marck Script' => 'Marck Script',
        'Margarine' => 'Margarine',
        'Marko One' => 'Marko One',
        'Marmelad' => 'Marmelad',
        'Marvel' => 'Marvel',
        'Mate' => 'Mate',
        'Mate SC' => 'Mate SC',
        'Maven Pro' => 'Maven Pro',
        'McLaren' => 'McLaren',
        'Meddon' => 'Meddon',
        'MedievalSharp' => 'MedievalSharp',
        'Medula One' => 'Medula One',
        'Megrim' => 'Megrim',
        'Meie Script' => 'Meie Script',
        'Merienda' => 'Merienda',
        'Merienda One' => 'Merienda One',
        'Merriweather' => 'Merriweather',
        'Merriweather Sans' => 'Merriweather Sans',
        'Metal' => 'Metal',
        'Metal Mania' => 'Metal Mania',
        'Metamorphous' => 'Metamorphous',
        'Metrophobic' => 'Metrophobic',
        'Michroma' => 'Michroma',
        'Milonga' => 'Milonga',
        'Miltonian' => 'Miltonian',
        'Miltonian Tattoo' => 'Miltonian Tattoo',
        'Miniver' => 'Miniver',
        'Miss Fajardose' => 'Miss Fajardose',
        'Modern Antiqua' => 'Modern Antiqua',
        'Molengo' => 'Molengo',
        'Molle' => 'Molle',
        'Monda' => 'Monda',
        'Monofett' => 'Monofett',
        'Monoton' => 'Monoton',
        'Monsieur La Doulaise' => 'Monsieur La Doulaise',
        'Montaga' => 'Montaga',
        'Montez' => 'Montez',
        'Montserrat' => 'Montserrat',
        'Montserrat Alternates' => 'Montserrat Alternates',
        'Montserrat Subrayada' => 'Montserrat Subrayada',
        'Moul' => 'Moul',
        'Moulpali' => 'Moulpali',
        'Mountains of Christmas' => 'Mountains of Christmas',
        'Mouse Memoirs' => 'Mouse Memoirs',
        'Mr Bedfort' => 'Mr Bedfort',
        'Mr Dafoe' => 'Mr Dafoe',
        'Mr De Haviland' => 'Mr De Haviland',
        'Mrs Saint Delafield' => 'Mrs Saint Delafield',
        'Mrs Sheppards' => 'Mrs Sheppards',
        'Muli' => 'Muli',
        'Mystery Quest' => 'Mystery Quest',
        'Neucha' => 'Neucha',
        'Neuton' => 'Neuton',
        'New Rocker' => 'New Rocker',
        'News Cycle' => 'News Cycle',
        'Niconne' => 'Niconne',
        'Nixie One' => 'Nixie One',
        'Nobile' => 'Nobile',
        'Nokora' => 'Nokora',
        'Norican' => 'Norican',
        'Nosifer' => 'Nosifer',
        'Nothing You Could Do' => 'Nothing You Could Do',
        'Noticia Text' => 'Noticia Text',
        'Noto Sans' => 'Noto Sans',
        'Noto Serif' => 'Noto Serif',
        'Nova Cut' => 'Nova Cut',
        'Nova Flat' => 'Nova Flat',
        'Nova Mono' => 'Nova Mono',
        'Nova Oval' => 'Nova Oval',
        'Nova Round' => 'Nova Round',
        'Nova Script' => 'Nova Script',
        'Nova Slim' => 'Nova Slim',
        'Nova Square' => 'Nova Square',
        'Numans' => 'Numans',
        'Nunito' => 'Nunito',
        'Odor Mean Chey' => 'Odor Mean Chey',
        'Offside' => 'Offside',
        'Old Standard TT' => 'Old Standard TT',
        'Oldenburg' => 'Oldenburg',
        'Oleo Script' => 'Oleo Script',
        'Oleo Script Swash Caps' => 'Oleo Script Swash Caps',
        'Open Sans' => 'Open Sans',
        'Open Sans Condensed' => 'Open Sans Condensed',
        'Oranienbaum' => 'Oranienbaum',
        'Orbitron' => 'Orbitron',
        'Oregano' => 'Oregano',
        'Orienta' => 'Orienta',
        'Original Surfer' => 'Original Surfer',
        'Oswald' => 'Oswald',
        'Over the Rainbow' => 'Over the Rainbow',
        'Overlock' => 'Overlock',
        'Overlock SC' => 'Overlock SC',
        'Ovo' => 'Ovo',
        'Oxygen' => 'Oxygen',
        'Oxygen Mono' => 'Oxygen Mono',
        'PT Mono' => 'PT Mono',
        'PT Sans' => 'PT Sans',
        'PT Sans Caption' => 'PT Sans Caption',
        'PT Sans Narrow' => 'PT Sans Narrow',
        'PT Serif' => 'PT Serif',
        'PT Serif Caption' => 'PT Serif Caption',
        'Pacifico' => 'Pacifico',
        'Paprika' => 'Paprika',
        'Parisienne' => 'Parisienne',
        'Passero One' => 'Passero One',
        'Passion One' => 'Passion One',
        'Pathway Gothic One' => 'Pathway Gothic One',
        'Patrick Hand' => 'Patrick Hand',
        'Patrick Hand SC' => 'Patrick Hand SC',
        'Patua One' => 'Patua One',
        'Paytone One' => 'Paytone One',
        'Peralta' => 'Peralta',
        'Permanent Marker' => 'Permanent Marker',
        'Petit Formal Script' => 'Petit Formal Script',
        'Petrona' => 'Petrona',
        'Philosopher' => 'Philosopher',
        'Piedra' => 'Piedra',
        'Pinyon Script' => 'Pinyon Script',
        'Pirata One' => 'Pirata One',
        'Plaster' => 'Plaster',
        'Play' => 'Play',
        'Playball' => 'Playball',
        'Playfair Display' => 'Playfair Display',
        'Playfair Display SC' => 'Playfair Display SC',
        'Podkova' => 'Podkova',
        'Poiret One' => 'Poiret One',
        'Poller One' => 'Poller One',
        'Poly' => 'Poly',
        'Pompiere' => 'Pompiere',
        'Pontano Sans' => 'Pontano Sans',
        'Port Lligat Sans' => 'Port Lligat Sans',
        'Port Lligat Slab' => 'Port Lligat Slab',
        'Prata' => 'Prata',
        'Preahvihear' => 'Preahvihear',
        'Press Start 2P' => 'Press Start 2P',
        'Princess Sofia' => 'Princess Sofia',
        'Prociono' => 'Prociono',
        'Prosto One' => 'Prosto One',
        'Puritan' => 'Puritan',
        'Purple Purse' => 'Purple Purse',
        'Quando' => 'Quando',
        'Quantico' => 'Quantico',
        'Quattrocento' => 'Quattrocento',
        'Quattrocento Sans' => 'Quattrocento Sans',
        'Questrial' => 'Questrial',
        'Quicksand' => 'Quicksand',
        'Quintessential' => 'Quintessential',
        'Qwigley' => 'Qwigley',
        'Racing Sans One' => 'Racing Sans One',
        'Radley' => 'Radley',
        'Raleway' => 'Raleway',
        'Raleway Dots' => 'Raleway Dots',
        'Rambla' => 'Rambla',
        'Rammetto One' => 'Rammetto One',
        'Ranchers' => 'Ranchers',
        'Rancho' => 'Rancho',
        'Rationale' => 'Rationale',
        'Redressed' => 'Redressed',
        'Reenie Beanie' => 'Reenie Beanie',
        'Revalia' => 'Revalia',
        'Ribeye' => 'Ribeye',
        'Ribeye Marrow' => 'Ribeye Marrow',
        'Righteous' => 'Righteous',
        'Risque' => 'Risque',
        'Roboto' => 'Roboto',
        'Roboto Condensed' => 'Roboto Condensed',
        'Roboto Slab' => 'Roboto Slab',
        'Rochester' => 'Rochester',
        'Rock Salt' => 'Rock Salt',
        'Rokkitt' => 'Rokkitt',
        'Romanesco' => 'Romanesco',
        'Ropa Sans' => 'Ropa Sans',
        'Rosario' => 'Rosario',
        'Rosarivo' => 'Rosarivo',
        'Rouge Script' => 'Rouge Script',
        'Rubik Mono One' => 'Rubik Mono One',
        'Rubik One' => 'Rubik One',
        'Ruda' => 'Ruda',
        'Rufina' => 'Rufina',
        'Ruge Boogie' => 'Ruge Boogie',
        'Ruluko' => 'Ruluko',
        'Rum Raisin' => 'Rum Raisin',
        'Ruslan Display' => 'Ruslan Display',
        'Russo One' => 'Russo One',
        'Ruthie' => 'Ruthie',
        'Rye' => 'Rye',
        'Sacramento' => 'Sacramento',
        'Sail' => 'Sail',
        'Salsa' => 'Salsa',
        'Sanchez' => 'Sanchez',
        'Sancreek' => 'Sancreek',
        'Sansita One' => 'Sansita One',
        'Sarina' => 'Sarina',
        'Satisfy' => 'Satisfy',
        'Scada' => 'Scada',
        'Schoolbell' => 'Schoolbell',
        'Seaweed Script' => 'Seaweed Script',
        'Sevillana' => 'Sevillana',
        'Seymour One' => 'Seymour One',
        'Shadows Into Light' => 'Shadows Into Light',
        'Shadows Into Light Two' => 'Shadows Into Light Two',
        'Shanti' => 'Shanti',
        'Share' => 'Share',
        'Share Tech' => 'Share Tech',
        'Share Tech Mono' => 'Share Tech Mono',
        'Shojumaru' => 'Shojumaru',
        'Short Stack' => 'Short Stack',
        'Siemreap' => 'Siemreap',
        'Sigmar One' => 'Sigmar One',
        'Signika' => 'Signika',
        'Signika Negative' => 'Signika Negative',
        'Simonetta' => 'Simonetta',
        'Sintony' => 'Sintony',
        'Sirin Stencil' => 'Sirin Stencil',
        'Six Caps' => 'Six Caps',
        'Skranji' => 'Skranji',
        'Slackey' => 'Slackey',
        'Smokum' => 'Smokum',
        'Smythe' => 'Smythe',
        'Sniglet' => 'Sniglet',
        'Snippet' => 'Snippet',
        'Snowburst One' => 'Snowburst One',
        'Sofadi One' => 'Sofadi One',
        'Sofia' => 'Sofia',
        'Sonsie One' => 'Sonsie One',
        'Sorts Mill Goudy' => 'Sorts Mill Goudy',
        'Source Code Pro' => 'Source Code Pro',
        'Source Sans Pro' => 'Source Sans Pro',
        'Source Serif Pro' => 'Source Serif Pro',
        'Special Elite' => 'Special Elite',
        'Spicy Rice' => 'Spicy Rice',
        'Spinnaker' => 'Spinnaker',
        'Spirax' => 'Spirax',
        'Squada One' => 'Squada One',
        'Stalemate' => 'Stalemate',
        'Stalinist One' => 'Stalinist One',
        'Stardos Stencil' => 'Stardos Stencil',
        'Stint Ultra Condensed' => 'Stint Ultra Condensed',
        'Stint Ultra Expanded' => 'Stint Ultra Expanded',
        'Stoke' => 'Stoke',
        'Strait' => 'Strait',
        'Sue Ellen Francisco' => 'Sue Ellen Francisco',
        'Sunshiney' => 'Sunshiney',
        'Supermercado One' => 'Supermercado One',
        'Suwannaphum' => 'Suwannaphum',
        'Swanky and Moo Moo' => 'Swanky and Moo Moo',
        'Syncopate' => 'Syncopate',
        'Tangerine' => 'Tangerine',
        'Taprom' => 'Taprom',
        'Tauri' => 'Tauri',
        'Telex' => 'Telex',
        'Tenor Sans' => 'Tenor Sans',
        'Text Me One' => 'Text Me One',
        'The Girl Next Door' => 'The Girl Next Door',
        'Tienne' => 'Tienne',
        'Tinos' => 'Tinos',
        'Titan One' => 'Titan One',
        'Titillium Web' => 'Titillium Web',
        'Trade Winds' => 'Trade Winds',
        'Trocchi' => 'Trocchi',
        'Trochut' => 'Trochut',
        'Trykker' => 'Trykker',
        'Tulpen One' => 'Tulpen One',
        'Ubuntu' => 'Ubuntu',
        'Ubuntu Condensed' => 'Ubuntu Condensed',
        'Ubuntu Mono' => 'Ubuntu Mono',
        'Ultra' => 'Ultra',
        'Uncial Antiqua' => 'Uncial Antiqua',
        'Underdog' => 'Underdog',
        'Unica One' => 'Unica One',
        'UnifrakturCook' => 'UnifrakturCook',
        'UnifrakturMaguntia' => 'UnifrakturMaguntia',
        'Unkempt' => 'Unkempt',
        'Unlock' => 'Unlock',
        'Unna' => 'Unna',
        'VT323' => 'VT323',
        'Vampiro One' => 'Vampiro One',
        'Varela' => 'Varela',
        'Varela Round' => 'Varela Round',
        'Vast Shadow' => 'Vast Shadow',
        'Vibur' => 'Vibur',
        'Vidaloka' => 'Vidaloka',
        'Viga' => 'Viga',
        'Voces' => 'Voces',
        'Volkhov' => 'Volkhov',
        'Vollkorn' => 'Vollkorn',
        'Voltaire' => 'Voltaire',
        'Waiting for the Sunrise' => 'Waiting for the Sunrise',
        'Wallpoet' => 'Wallpoet',
        'Walter Turncoat' => 'Walter Turncoat',
        'Warnes' => 'Warnes',
        'Wellfleet' => 'Wellfleet',
        'Wendy One' => 'Wendy One',
        'Wire One' => 'Wire One',
        'Yanone Kaffeesatz' => 'Yanone Kaffeesatz',
        'Yellowtail' => 'Yellowtail',
        'Yeseva One' => 'Yeseva One',
        'Yesteryear' => 'Yesteryear',
        'Zeyada' => 'Zeyada',

    );

      /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Crexis', 'crexis' ),
        'page_title'           => esc_html__( 'Crexis Theme Options', 'crexis' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyBC3L1znn2yrtdsXm9hEryNjbBlWbjrewrwe5nEk',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
		'templates_path'		=> get_template_directory() . '/framework/theme-panel/crexis/templates/panel/',
        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'crexis' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'crexis' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'crexis' ),
    );

    // Add content after the form.
    $args['footer_text'] = '<p>Need help? Visit our dedicated <a href="http://veented.com/support" target="_blank">Support Forums</a>.</p>';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'crexis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'crexis' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'crexis' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'crexis' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'crexis' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
     
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'General', 'crexis' ),
        'id'     => 'general',
        'desc'   => esc_html__( 'General Theme Settings.', 'crexis' ),
        'icon'   => 'fa fa-home',
        'fields' => array(
//            array(
//                'id'       => 'site_logo',
//                'type'     => 'media',
//                'url'      => true,
//                'title'    => esc_html__( 'Website Logo Image', 'crexis' ),
//                'compiler' => 'true',
//                'subtitle' => esc_html__( "Upload your website's logo image.", 'crexis' ),
//                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-dark.png' ),
//            ),
            array(
                'id'       => 'site_logo_main',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Website Logo Image', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Upload your website's logo image.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-dark.png' ),
            ),
            array(
                'id'       => 'site_logo_white',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'White Logo Image Version', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Used for Transparent Header style.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-white.png' ),
            ),
            array(
                'id'       => 'stt',
                'type'     => 'switch',
                'title'    => esc_html__( 'Scroll to Top Button', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Scroll to Top button on your website.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'default_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Default Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis' ),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'fullwidth'
            ),
            array(
                'id'       => 'parallax_plugin',
                'type'     => 'select',
                'title'    => esc_html__( 'Parallax Plugin', 'crexis' ),
                'subtitle' => esc_html__( "Choose a plugin to be used for Visual Composer row's image backgrounds.", 'crexis' ),
                'options'  => array(
                    "crexis" => "Default Crexis Plugin",
                    "vc" 	=> "Native Visual Composer Plugin"
                ),
                'default'  => 'yes'
            ),
            array(
                'id'       => 'pageloader',
                'type'     => 'select',
                'title'    => esc_html__( 'Page Loader', 'crexis' ),
                'subtitle' => esc_html__( 'Enable or disable the page loading icon.', 'crexis' ),
                'options'  => array(
                    "yes" => "Yes",
                    "no" 	=> "No"
                ),
                'default'  => 'yes'
            ),
        )
    ) );
    
    // Header Tab
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Header', 'crexis' ),
        'id'     => 'header',
        'desc'   => esc_html__( 'Header Settings.', 'crexis' ),
        'icon'   => 'fa fa-columns',
        'fields' => array(
            array(
                'id'       => 'header_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style of your Site Header.', 'crexis' ),
                'options'  => array(
                    'style-default' => 'Classic',
                    'style-transparent' => 'Transparent',
                    'style-transparent-hamburger' => 'Transparent Hamburger Menu',
                    'disable' => 'Disable',
                ),
                'default'  => 'style-default'
            ),
            array(
                'id'       => 'header_color',
                'type'     => 'select',
                'title'    => esc_html__( 'Header Color', 'crexis' ),
                'subtitle' => esc_html__( 'Choose color scheme for your Header.', 'crexis' ),
                'options'  => array(
                    "white" => "White",
                    "dark" 	=> "Dark"
                ),
                'default'  => 'white'
            ),
            array(
                'id'       => 'header_dropdown_color',
                'type'     => 'select',
                'title'    => esc_html__( 'Dropdown Menu Color', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a color scheme for the dropdown menu.', 'crexis' ),
                'options'  => array(
                    "white" => "White",
                    "dark" 	=> "Dark"
                ),
                'default'  => 'white'
            ),
            array(
                'id'       => 'header_mobile_sticky',
                'type'     => 'switch',
                'title'    => esc_html__( 'Header Sticky Mobile', 'crexis' ),
                'subtitle' => esc_html__( 'Enable this option to make the header sticky on mobile devices.', 'crexis' ),
                'default'  => false,
            ),
            array(
                'id'       => 'header_search',
                'type'     => 'switch',
                'title'    => esc_html__( 'Header Search', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Search field in Header.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'header_title',
                'type'     => 'switch',
                'title'    => esc_html__( 'Page Title', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Page Title area globally.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'breadcrumbs',
                'type'     => 'switch',
                'title'    => esc_html__( 'Breadcrumbs', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Breadcrumbs navigation.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'pagetitle_parallax',
                'type'     => 'select',
                'title'    => esc_html__( 'Page Title Background Parallax', 'crexis' ),
                'subtitle' => esc_html__( 'Enable or disable the parallax effect for a custom page title background.', 'crexis' ),
                'options'  => array(
                    "yes" => "Yes",
                    "no" 	=> "No"
                ),
                'default'  => 'yes'
            ),
            
        )
    ) );
    
    // Top Bar
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Top Bar', 'crexis' ),
        'id'     => 'topbar',
        'desc'   => esc_html__( 'Top Bar Settings.', 'crexis' ),
        'icon'   => 'fa fa-columns',
        'fields' => array(
            array(
                'id'       => 'topbar',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Bar', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Top Bar section.', 'crexis' ),
                'default'  => true,
            ),
            array(
                'id'       => 'topbar_force',
                'type'     => 'switch',
                'title'    => esc_html__( 'Top Bar in Transparent Header', 'crexis' ),
                'subtitle' => esc_html__( 'Show the Top Bar section with Transparent Header style.', 'crexis' ),
                'default'  => false,
            ),
            array(
                'id'       => 'topbar_left',
                'type'     => 'select',
                'title'    => esc_html__( 'Left side content type', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a content type for the left side of the Top Bar section.', 'crexis' ),
                'options'  => array(
                    "social" => "Social Icons",
//                    "menu" => "Menu",
                    "text" => "Text",
                    "textsocial" => "Text + Social Icons"
                ),
                'default'  => 'text'
            ),
            array(
                'id'       => 'topbar_right',
                'type'     => 'select',
                'title'    => esc_html__( 'Right side content type', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a content type for the right side of the Top Bar section.', 'crexis' ),
                'options'  => array(
                    "social" => "Social Icons",
//                    "menu" => "Menu",
                    "text" => "Text",
                    "textsocial" => "Text + Social Icons"
                ),
                'default'  => 'social'
            ),
            array(
                'id'       => 'topbar_text_left',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Left Top Bar Text', 'crexis' ),
                'subtitle' => esc_html__( 'The text content that is being selectable as one of the "content types" for the Top Bar. Supports HTML.', 'crexis' ),
                'default'  => 'E-Mail: hello@crexis.com Phone: 591 341 344',
            ),
            array(
                'id'       => 'topbar_text_right',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Right Top Bar Text', 'crexis' ),
                'subtitle' => esc_html__( 'The text content that is being selectable as one of the "content types" for the Top Bar. Supports HTML.', 'crexis' ),
                'default'  => '[icon icon="envelope"] hello@crexis.com [icon icon="phone"] 591 341 344',
            )
            
    	)
    ) );
    
    // Footer
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Footer', 'crexis' ),
        'id'     => 'footer',
        'desc'   => esc_html__( 'Footer Settings.', 'crexis' ),
        'icon'   => 'fa fa-download',
        'fields' => array(
            array(
                'id'       => 'copyright',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Copyright Text', 'crexis' ),
                'subtitle' => esc_html__( 'Copyright text displayed in the footer.', 'crexis' ),
                'default'  => 'Copyright 2017 All rights reserved. Designed by (link)My Website(/link).',
            ),
            array(
                'id'       => 'copyright_url',
                'type'     => 'text',
                'title'    => esc_html__( 'Copyright Link URL', 'crexis' ),
                'subtitle' => esc_html__( 'Link for the (link) shortcode in the copyright text.', 'crexis' ),
                'default'  => 'http://yoursite.url',
            ),
            array(
                'id'       => 'footer_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Footer Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style of your footer.', 'crexis' ),
                'options'  => array(
                    "classic" => "Classic (copyright + social icons)",
                    "centered" => "Centered with Image"
                ),
                'default'  => 'classic'
            ),
            array(
                'id'       => 'footer_logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Footer Logo Image', 'crexis' ),
                'compiler' => 'true',
                'subtitle' => esc_html__( "Upload an image that will be displayed above the copyright text.", 'crexis' ),
                'default'  => array( 'url' => get_template_directory_uri() . '/img/logo-white.png' ),
                'required' => array('footer_style','=',"centered")
            ),
            array(
                'id'       => 'footer_widgets',
                'type'     => 'switch',
                'title'    => esc_html__( 'Footer Widgets Area', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Footer Widgets area globally. Please visit Appearance / Widgets menu to add new widgets!', 'crexis' ),
                'default'  => true,
            ),
//            array(
//                'id'       => 'footer_column_margin',
//                'type'     => 'text',
//                'title'    => esc_html__( 'First column top margin', 'crexis' ),
//                'subtitle' => esc_html__( 'Set a top margin for the first column of the Widgets Area. Handy if you want to vertically center the first column content (for example with a logo image) with the rest of the columns. Example: -20px.', 'crexis'),
//                'default'  => '-27px',
//            ),
            
    	)
    ) );    
    
    // Blog
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Blog', 'crexis' ),
        'id'     => 'blog',
        'desc'   => esc_html__( 'Blog Settings.', 'crexis' ),
        'icon'   => 'fa fa-file-text-o',
        'fields' => array(
            array(
                'id'       => 'blog_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style for your Blog.', 'crexis' ),
                'options'  => array(
                    "classic" => "Classic",
                    "grid" => "Grid",
                    "minimal" => "Minimal",
                ),
                'default'  => 'classic'
            ),
            array(
                'id'       => 'blog_meta_style',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Meta Style', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a style for your meta data (date, author, categories etc).', 'crexis' ),
                'options'  => array(
                    "default" => "Default - aside",
                    "classic" => "Classic - below Featured Image",
                ),
                'default'  => 'default'
            ),
            array(
                'id'       => 'blog_masonry',
                'type'     => 'switch',
                'title'    => esc_html__( 'Masonry', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Masonry effect for your grid.', 'crexis' ),
                'default'  => true,
                'required' => array('blog_style','=',"grid")
            ),
            array(
                'id'       => 'blog_grid_cols',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Grid Columns', 'crexis' ),
                'subtitle' => esc_html__( 'Select number of columns for your grid.', 'crexis' ),
                'options'  => array(
                	"6" => "6",
                	"5" => "5",
                    "4" => "4",
                    "3" => "3",
                    "2" => "2",
                ),
                'default'  => '3',
                'required' => array('blog_style','=',"grid")
            ),
            array(
                'id'       => 'blog_fullwidth',
                'type'     => 'switch',
                'title'    => esc_html__( 'Fullwidth', 'crexis' ),
                'subtitle' => esc_html__( 'Check this option to enable a 100% width blog page.', 'crexis' ),
                'default'  => false,
                'required' => array( 'blog_style', '=', "grid" )
            ),
            array(
                'id'       => 'blog_ajax',
                'type'     => 'switch',
                'title'    => esc_html__( 'Ajax Pagination', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the Ajax Pagination.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_media_link_type',
                'type'     => 'select',
                'title'    => esc_html__( 'Blog Index Image Behaviour', 'crexis' ),
                'subtitle' => esc_html__( 'Choose if image should open a lightbox or link to the post page.', 'crexis' ),
                'options'  => array(
                    "lightbox" => esc_html__( "Open lightbox", 'crexis' ),
                    "link" => esc_html__( "Open post page", 'crexis' ),
                ),
            ),
            array(
                'id'       => 'blog_meta_author',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Post Author', 'crexis' ),
                'subtitle' => esc_html__( 'Display author in the blog post meta.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta_comments',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Comments Count', 'crexis' ),
                'subtitle' => esc_html__( 'Display the blog post comments count.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta_categories',
                'type'     => 'switch',
                'title'    => esc_html__( 'Display Post Categories', 'crexis' ),
                'subtitle' => esc_html__( 'Display the post categories.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_trackback',
                'type'     => 'switch',
                'title'    => esc_html__( 'Post Trackback', 'crexis' ),
                'subtitle' => esc_html__( 'Display the post trackback URL.', 'crexis' ),
                'default'  => false
            ),
            array(
                'id'       => 'blog_post_nav',
                'type'     => 'switch',
                'title'    => esc_html__( 'Post Navigation', 'crexis' ),
                'subtitle' => esc_html__( 'Display links for next/previous posts on single blog post pages.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_meta',
                'type'     => 'switch',
                'title'    => esc_html__( 'Blog Index Meta Section', 'crexis' ),
                'subtitle' => esc_html__( 'Enable the meta section on blog index page.', 'crexis' ),
                'default'  => true
            ),
            array(
                'id'       => 'blog_single_meta',
                'type'     => 'switch',
                'title'    => esc_html__( 'Single Post Meta Section', 'crexis' ),
                'subtitle' => esc_html__( 'Enable the  blog post meta section on individual post page.', 'crexis' ),
                'default'  => true
            ),
            
    	)
    ) );    
    
    // Portfolio
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Portfolio', 'crexis' ),
        'id'     => 'portfolio',
        'desc'   => esc_html__( 'Portfolio Settings.', 'crexis' ),
        'icon'   => 'fa fa-briefcase',
        'fields' => array(
            array(
                'id'       => 'portfolio_url',
                'type'     => 'select',
                'data'     => 'pages',
                'title'    => esc_html__( 'Main Portfolio Page', 'crexis' ),
                'subtitle' => esc_html__( 'Select a default portfolio page for the "Back to portfolio" link on single portfolio posts.', 'crexis' ),
            ),
            
    	)
    ) );
    
    // Sidebars
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Sidebars', 'crexis' ),
        'id'     => 'sidebars',
        'icon'   => 'fa fa-indent',
        'fields' => array(
            array(
                'id'       => 'sidebar_generator',
                'type'     => 'multi_text',
                'title'    => esc_html__( 'Sidebar Manager', 'crexis' ),
                'subtitle' => esc_html__( 'Create new sidebars.', 'crexis' ),
            ),
            
    	)
    ) );
    
    // Social Icons
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Social Icons', 'crexis' ),
        'id'     => 'socialicons',
        'icon'   => 'fa fa-twitter',
        'fields' => array(
            array(
                'id'       => 'social_icons',
                'type'     => 'sortable',
                'title'    => esc_html__( 'Social Icons Generator', 'crexis' ),
                'subtitle' => esc_html__( 'Easily add and arrange social icons for use in Footer and Top Bar.<br>Leave blank field to disable a specific icon.', 'crexis' ),
                'label'    => true,
                'options'  => array(
                    'Facebook URL' => 'http://your_facebook_page_url',
                    'Twitter' => '#',
                    'Google-plus' => '',
                    'Linkedin' => '',
                    'Dribbble' => '#',
                    'Vimeo' => '#',
                    'Youtube' => '#',
                    'Pinterest' => '',
                    'Skype' => '',
                    'Tumblr' => '',
                    'Dropbox' => '',
                    'RSS' => '',
                    'Weibo' => '',
                    'Flickr' => '',
                    'Instagram' => '',
                    'Stack-exchange' => '',
                    'Stack-overflow' => '',
                    'Github' => '',
                    'Maxcdn' => '',
                    'Behance' => '',
                    'Soundcloud' => '',
                    'E-mail' => ''
                )
            ),
            
    	)
    ) );
    
    // Appearance
    
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Appearance', 'crexis' ),
        'id'    => 'appearance',
        'icon'  => 'fa fa-paint-brush'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General', 'crexis' ),
        'id'         => 'appearance_general',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'accent_color',
                'type'     => 'color',
                'title'    => esc_html__( 'Accent Color', 'crexis' ),
                'subtitle' => esc_html__( 'Main Accent color.', 'crexis' ),
                'default'  => '#e54343',
                'transparent' => false
            ),
            array(
                'id'       => 'accent_color2',
                'type'     => 'color',
                'title'    => esc_html__( 'Accent Color 2', 'crexis' ),
                'subtitle' => esc_html__( 'Main Accent color.', 'crexis' ),
                'default'  => '#f35050',
                'transparent' => false
            ),
			array(
			    'id'       => 'accent_color3',
			    'type'     => 'color',
			    'title'    => esc_html__( 'Accent Color 3', 'crexis' ),
			    'subtitle' => esc_html__( 'Alternative Accent color.', 'crexis' ),
			    'default'  => '#222222',
			    'transparent' => false
			),
			array(
			    'id'       => 'bg_color',
			    'type'     => 'color',
			    'title'    => esc_html__( 'Background Color', 'crexis' ),
			    'default'  => '#ffffff',
			    'transparent' => false
			),
			array(
			    'id'       => 'theme_skin',
			    'type'     => 'select',
			    'title'    => esc_html__( 'Theme Skin', 'crexis' ),
			    'subtitle' => esc_html__( 'Choose a skin.', 'crexis' ),
			    'options'  => array(
			        "white" => "White",
			        "dark" => "Dark",
			        "night" => "Night",
			    ),
			    'default'  => 'white'
			),
			array(
			    'id'       => 'predefined_colors',
			    'type'     => 'palette',
			    'title'    => esc_html__( 'Predefined Color Palettes', 'crexis' ),
			    'subtitle' => esc_html__( 'Select a predefined scheme for the accent color.', 'crexis' ),
			    'default'  => 'red',
			    'palettes' => array(
			        'red'  => array( // Original red
			            '#e54343',
			            '#f35050',
			            '#222222',
			        ),
			        'orange'  => array(
			            '#ffa800',
			            '#FFB423',
			            '#222222'
			        ),
			        'green'  => array( // Original emerald
			            '#7bcba8',
			            '#00a652',
			        ),
			        'green-bright'  => array( // Brighter Green
			            '#8dc63f',
			            '#7ab22f',
			        ),
			        'blue'  => array(
			            '#00bff3',
			            '#00abe4',
			        ),  
			        
			    )
			),
        ),
        
    ) );
    
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Header', 'crexis' ),
            'id'         => 'appearance_header',
            'subsection' => true,
            'fields'     => array(
				array(
				    'id'       => 'color_navigation',
				    'type'     => 'color',
				    'title'    => esc_html__( 'Site Navigation Text Color', 'crexis' ),
				    'subtitle' => esc_html__( 'Choose a color for your main site navigation links.', 'crexis' ),
				    'default'  => '#3a3a3a',
				    'transparent' => false
				),
    			array(
    			    'id'       => 'topbar_bg_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Top Bar Background Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Background color of the Top Bar section.', 'crexis' ),
    			    'default'  => '#ffffff',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Page Title', 'crexis' ),
            'id'         => 'appearance_pagetitle',
            'subsection' => true,
            'fields'     => array(
    			array(
    			    'id'       => 'breadcrumbs_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Breadcrumbs Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Text color of the Breadcrumbs navigation.', 'crexis' ),
    			    'default'  => '#959595',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Page Content', 'crexis' ),
            'id'         => 'appearance_pagecontent',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'       => 'body_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Text Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Website paragraph text color.', 'crexis' ),
                    'default'  => '#888888',
                    'transparent' => false
                ),
                array(
                    'id'       => 'link_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Link Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Leave blank to use accent color.', 'crexis' ),
                    'default'  => '',
                    'transparent' => false
                ),
                array(
                    'id'       => 'link_hover_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Link Hover Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Basic link hover color.', 'crexis' ),
                    'default'  => '#333333',
                    'transparent' => false
                ),
    			array(
    			    'id'       => 'heading_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Heading Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Color of text Headings.', 'crexis' ),
    			    'default'  => '#666666',
    			    'transparent' => false
    			),
    			array(
    			    'id'       => 'special_heading_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Special Heading Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Color of text Headings.', 'crexis' ),
    			    'default'  => '#666666',
    			    'transparent' => false
    			),
            ),
            
        ) );
        
    Redux::setSection( $opt_name, array(
            'title'      => esc_html__( 'Footer', 'crexis' ),
            'id'         => 'appearance_footer',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'       => 'footer_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Text Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Footer text color.', 'crexis' ),
                    'default'  => '#666666',
                    'transparent' => false
                ),
                array(
                    'id'       => 'footer_bg_color',
                    'type'     => 'color',
                    'title'    => esc_html__( 'Footer Background Color', 'crexis' ),
                    'subtitle' => esc_html__( 'Background color of the Footer.', 'crexis' ),
                    'default'  => '#111111',
                    'transparent' => false
                ),
    			array(
    			    'id'       => 'footer_widgets_bg_color',
    			    'type'     => 'color',
    			    'title'    => esc_html__( 'Footer Widgets Background Color', 'crexis' ),
    			    'subtitle' => esc_html__( 'Background color of the Footer Widgets.', 'crexis' ),
    			    'default'  => '#191919',
    			    'transparent' => false
    			),
            ),
            
        ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Typography', 'crexis' ),
        'id'         => 'appearance_typography',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'typography_body',
                'type'     => 'typography',
                'title'    => esc_html__( 'Body Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the body font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-style" => false,
                "color" => false,
                'default'  => array(
                    'font-size'   => '14px',
                    'font-family' => 'Open Sans',
                    'font-weight' => '400',
                ),
            ),
            array(
                'id'       => 'typography_primary',
                'type'     => 'typography',
                'title'    => esc_html__( 'Primary Heading Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the primary heading font.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-size" => false,
                "font-style" => false,
                "color" => false,
                "text-transform" => true,
                'default'  => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '500',
                    "font-size" => "36px",
                    "text-transform" => "none"
                ),
            ),
            array(
                'id'       => 'typography_secondary',
                'type'     => 'typography',
                'title'    => esc_html__( 'Secondary Heading Font', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the secondary heading font.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
                "font-size" => false,
                "color" => false,
                "font-weight" => false,
                "subsets" => false,
                "font-style" => false,
                "text-transform" => false,
                'default'  => array(
                    'color'       => '#333333',
                    'font-family' => 'Oswald',
                    'font-weight' => '400',
                    "font-size" => "36px",
                    "text-transform" => "none"
                ),
            ),
            array(
                'id'       => 'typography_navigation_font',
                'type'     => 'select',
                'title'    => esc_html__( 'Site Navigation Font Family', 'crexis' ),
                'subtitle' => esc_html__( 'Select the font family for the site navigation.', 'crexis' ),
                'options'  => array(
                	"body" => "Body Font Family",
                	"heading" => "Primary Heading Font Family",
                ),
                'default'  => 'body'
            ),
            array(
                'id'       => 'typography_navigation',
                'type'     => 'typography',
                'title'    => esc_html__( 'Site Navigation Font Properties', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the site navigation font properties.', 'crexis' ),
                "font-family" => false,
                'google'   => false,
                "text-align" => false,
                "letter-spacing" => true,
                "font-weight" => true,
                "font-style" => false,
                "line-height" => false,
                "subsets" => false,
                "text-transform" => true,
                "color" => false,
                'default'  => array(
                    'font-weight' => '500',
                    "font-size" => "11px",
                    "text-transform" => "none",
                    "letter-spacing" => "0px"
                ),
            ),
            array(
                'id'       => 'typography_special_heading',
                'type'     => 'typography',
                'title'    => esc_html__( 'Special Heading', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the special heading font properties.', 'crexis' ),
                "font-family" => false,
                'google'   => false,
                "text-align" => false,
                "letter-spacing" => true,
                "font-weight" => true,
                "font-style" => false,
                "line-height" => false,
                "subsets" => false,
                "text-transform" => true,
                "color" => false,
                'default'  => array(
                    'font-weight' => '600',
                    "font-size" => "30px",
                    "text-transform" => "uppercase"
                ),
            ),
            array(
                'id'       => 'typography_h1',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 1 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
				"color" => false,
				"font-family" => false,
				"font-style" => false,
				"font-weight" => false,
                'default'  => array(
                    "font-size" => "36px"
                ),
            ),
            array(
                'id'       => 'typography_h2',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 2 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "30px"
                ),
            ),
            array(
                'id'       => 'typography_h3',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 3 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "26px"
                ),
            ),
            array(
                'id'       => 'typography_h4',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 4 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "24px"
                ),
            ),
            array(
                'id'       => 'typography_h5',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 5 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "21px"
                ),
            ),
            array(
                'id'       => 'typography_h6',
                'type'     => 'typography',
                'title'    => esc_html__( 'Heading 6 Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the heading font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "18px"
                ),
            ),
            array(
                'id'       => 'typography_page_title',
                'type'     => 'typography',
                'title'    => esc_html__( 'Page Title', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the page title font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
            	'color' => false,
                'default'  => array(
                    "font-size" => "30px",
                    "font-weight" => '300'
                ),
            ),
            array(
                'id'       => 'typography_copyright',
                'type'     => 'typography',
                'title'    => esc_html__( 'Copyright Text Font Size', 'crexis' ),
                'subtitle' => esc_html__( 'Specify the copyright section font properties.', 'crexis' ),
                'google'   => true,
                "text-align" => false,
                "line-height" => false,
            	"color" => false,
            	"font-family" => false,
            	"font-style" => false,
            	"font-weight" => false,
                'default'  => array(
                    "font-size" => "13px"
                ),
            ),
        )
    ) );
    
    // Archives/Search
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Archives/Search', 'crexis' ),
        'id'     => 'archives',
        'icon'   => 'fa fa-search',
        'fields' => array(
            array(
                'id'       => 'archives_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Archives Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis'),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'sidebar_right'
            ),
            array(
                'id'       => 'search_layout',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Search Page Layout', 'crexis' ),
                'subtitle' => esc_html__( 'Choose a default page layout for your pages: Fullwidth, Sidebar Right or Sidebar Left', 'crexis' ),
                'options'  => array(
                    'fullwidth' => array(
                        'alt' => '1 Column',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    'sidebar_left' => array(
                        'alt' => '2 Column Left',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'sidebar_right' => array(
                        'alt' => '2 Column Right',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                ),
                'default'  => 'sidebar_right'
            ),
            
    	)
    ) );
    
    //$true = true;
    
    //if( class_exists('Woocommerce') ) { // Enable this section only if the WooCommerce plugin is activated
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'WooCommerce', 'crexis' ),
        'id'     => 'woocommerce',
        'icon'   => 'fa fa-shopping-cart',
        'fields' => array(
            array(
                'id'       => 'header_woocommerce',
                'type'     => 'switch',
                'title'    => esc_html__( 'Shopping Cart Icon', 'crexis' ),
                'subtitle' => esc_html__( 'Enable/Disable the WooCommerce icon in the Header section.', 'crexis' ),
                'default'  => true
            ),
            
    	)
    ) );
    
    //}
    
    // Advanced 
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Advanced', 'crexis' ),
        'id'     => 'advanced',
        'icon'   => 'fa fa-wrench',
        'fields' => array(
            array(
                'id'       => 'custom_css',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Custom CSS Code', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your CSS code here.', 'crexis' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
                'default'  => "#header{\n   margin: 0 auto;\n}"
            ),
            array(
                'id'       => 'custom_js',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Custom JS Code', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your JavaScript code here.', 'crexis' ),
                'mode'     => 'javascript',
                'theme'    => 'chrome',
                'default'  => "jQuery(document).ready(function(){\n\n});"
            ),
            
    	)
    ) );
    
    // Google Maps API 
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Google Maps API', 'crexis' ),
        'id'     => 'apis',
        'icon'   => 'fa fa-map',
        'fields' => array(
            array(
                'id'       => 'google_maps_api',
                'type'     => 'text',
                'placeholder' => esc_html__( 'Your API key goes here..' , 'crexis' ),
                'title'    => esc_html__( 'Google Maps API Key', 'crexis' ),
                'subtitle' => esc_html__( 'Paste your Google Maps Api Key. For more information, check ', 'crexis' ) . '<a href="https://veented.ticksy.com/article/7856/" target="_blank">' . esc_html__('this article', 'crexis'). '</a>',
                'default'  => ""
            ),
            
    	)
    ) );
    
    // Extras
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Extras - NEW!', 'crexis' ),
        'id'     => 'extras',
        'icon'   => 'fa fa-rocket',
        'fields' => array(
            array(
                'id'    => 'extras_info',
                    'type'  => 'info',
                    'title' => __('Essential Grid and Ultimate VC Addons are now included with the theme!', 'crexis'),
                    'style' => 'info',
                    'desc'  => __('It means that you can use those awesome, super popular premium plugins for free with Crexis! Please check <a href="https://veented.ticksy.com/article/8523/" target="_blank">this article</a> for more information. Kind Regards!', 'crexis')
                )
                
            
    	)
    ) );

    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */


    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {

            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'crexis' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'crexis' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

